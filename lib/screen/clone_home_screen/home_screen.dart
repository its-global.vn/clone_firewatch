
import 'package:clone_firewatch/screen/clone_home_screen/clone_home_screen.dart';
import 'package:clone_firewatch/screen/screen.dart';
import 'package:flutter/material.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

import '../../widgets/widgets.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final videoId = YoutubePlayer.convertUrlToId(
      "https://www.youtube.com/watch?v=YB74iov4gG4");
  late YoutubePlayerController controller;

  @override
  void initState() {
    super.initState();
    controller = YoutubePlayerController(
      initialVideoId: videoId!,
      flags: const YoutubePlayerFlags(
        autoPlay: false,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.height * 1 / 2,
            child: Stack(
              children: [
                for (var i = 0; i <= 8; i++)
                  ParallaxImages(
                    imagePath: "assets/images/parallax/parallax$i.png",
                    height: MediaQuery.of(context).size.height * 3 / 4,
                    width: MediaQuery.of(context).size.width * 3 / 4,
                    index: i + 1,
                  ),
              ],
            ),
          ),
          const ListButtonDownload(),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: YoutubePlayer(
              controller: controller,
              showVideoProgressIndicator: true,
            ),
          ),
          const BodyScrip(),
          for (int i = 1; i <= 4; i++)
            Hero(
              tag: "Images",
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: InkWell(
                  child: Image(
                      image: AssetImage(
                          "assets/images/screenshots/thumbs/firewatch_0$i.jpg")),
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (context) => DetailImageScreen(index: i));
                  },
                ),
              ),
            ),
          const EndScrip(),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Image.asset(
                  "assets/images/logo_camposanto_transparent.png",
                  width: 100,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Container(
                    color: const Color.fromRGBO(151, 55, 0, 1),
                    child: Image.asset(
                      "assets/images/logo_panic_transparent.png",
                      width: 100,
                    )),
              ),
            ],
          )
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';

import '../../widgets/widgets.dart';
class BodyScrip extends StatefulWidget {
  const BodyScrip({Key? key}) : super(key: key);

  @override
  State<BodyScrip> createState() => _BodyScripState();
}

class _BodyScripState extends State<BodyScrip> {
  @override
  Widget build(BuildContext context) {
    return const Column(
      children: [
        Padding(
          padding:
          EdgeInsets.only(top: 40.0, bottom: 20, right: 20, left: 20),
          child: Align(
            alignment: Alignment.center,
            child: CustomText(
              tittle:
              "  Firewatch is a mystery set in the Wyoming wilderness, where your only emotional lifeline is the person on the other end of a handheld radio.",
            ),
          ),
        ),
        Padding(
          padding:
          EdgeInsets.only(top: 20.0, bottom: 20, right: 10, left: 30),
          child: Align(
            alignment: Alignment.center,
            child: CustomText(
              fontWeight: FontWeight.normal,
              size: 22,
              tittle:
              "   The year is 1989.\n\n   You are a man named Henry who has retreated from your messy life to work as a fire lookout in the Wyoming wilderness. Perched atop a mountain, it's your job to find smoke and keep the wilderness safe.\n\n   An especially hot, dry summer has everyone on edge. Your supervisor, a woman named Delilah, is available to youat all times over a small, handheld radio—and is your only contact with the world you've left behind.\n\n    But when something strange draws you out of your lookout tower and into the world below, you'll explore a wild and unknown environment, facing questions and making interpersonal choices that can build or destroy the only meaningful relationship you have.",
            ),
          ),
        ),
        CustomButton(
          tittle: "SCREEN & TRAILER",
          height: 60,
        ),
        CustomButton(
          tittle: "FIREWATCH FAQ",
          height: 60,
        ),
      ],
    );
  }
}

class EndScrip extends StatefulWidget {
  const EndScrip({Key? key}) : super(key: key);

  @override
  State<EndScrip> createState() => _EndScripState();
}

class _EndScripState extends State<EndScrip> {
  @override
  Widget build(BuildContext context) {
    return const Column(
      children: [
        Padding(
          padding: EdgeInsets.all(20.0),
          child: CustomText(
            tittle: "\"AS VISUALLY STRIKING\nAS ITS UNIQUE PREMISE\"",
            size: 28,
          ),
        ),
        CustomText(
          tittle: "ENTERTAINMENT WEEKLY",
          color: Color.fromRGBO(151, 55, 0, 1),
          size: 22,
        ),
        Padding(
          padding: EdgeInsets.only(top: 50.0),
          child: CustomButton(
            tittle: "TECH SUPPORT",
            height: 50,
          ),
        ),
        CustomButton(
          tittle: "STREAM AND LETS PLAY",
          height: 70,
        ),
        CustomText(tittle: "日本語に関する情報"),
        Padding(
          padding: EdgeInsets.all(30.0),
          child: CustomText(
              size: 12,
              color: Color.fromRGBO(151, 55, 0, 1),
              tittle:
              "©2023 CAMPO SANTO, IN COOPERATION WITH PANIC.\nFIREWATCH IS A TRADEMARK OF CAMPO SANTO.\nNINTENDO SWITCH IS A TRADEMARK OF NINTENDO."),
        ),
      ],
    );
  }
}


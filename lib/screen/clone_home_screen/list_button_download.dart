import 'package:flutter/material.dart';

import '../../widgets/widgets.dart';
class ListButtonDownload extends StatefulWidget {
  const ListButtonDownload({Key? key}) : super(key: key);

  @override
  State<ListButtonDownload> createState() => _ListButtonDownloadState();
}

class _ListButtonDownloadState extends State<ListButtonDownload> {
  @override
  Widget build(BuildContext context) {
    return const Column(
      children: [
        Padding(
          padding: EdgeInsets.all(20.0),
          child: Align(
            alignment: Alignment.center,
            child: CustomText(
              tittle: "AVAILABLE NOW FOR \$ 19.99",
            ),
          ),
        ),
        CustomButton(
          tittle: "WINDOWS MAC LINUX ",
          imageIconPath: "buyicon_pc.png",
        ),
        CustomButton(
          tittle: "PLAYSTATION 4",
          imageIconPath: "buyicon_ps4.png",
        ),
        CustomButton(
          tittle: "NiNTENDO SWITCH",
          imageIconPath: "buyicon_switch.png",
        ),
        CustomButton(
          tittle: "XBOX ONE",
          imageIconPath: "buyicon_xbone.png",
        ),
      ],
    );
  }
}

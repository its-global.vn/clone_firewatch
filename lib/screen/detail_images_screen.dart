import 'package:flutter/material.dart';
class DetailImageScreen extends StatefulWidget {
  final int index;
  const DetailImageScreen({Key? key, required this.index}) : super(key: key);

  @override
  State<DetailImageScreen> createState() => _DetailImageScreenState();
}

class _DetailImageScreenState extends State<DetailImageScreen> {
  late int index =1;
  @override
  void initState(){
    super.initState();
    index = widget.index;
  }
  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: "$index",
      child: Dialog(
        backgroundColor:
        Colors.transparent.withOpacity(0.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment:
          CrossAxisAlignment.center,
          children: [
            InkWell(
              child: const Icon(
                Icons.arrow_left,
                color: Colors.green,
              ),
              onTap: () {
                setState(() {
                  index = index ==1 ? 4 :index -1;
                });
              },
            ),
            InkWell(
              onTap: () => Navigator.pop(context),
              child: Image(
                image: AssetImage(
                    "assets/images/screenshots/thumbs/firewatch_0$index.jpg"),
                fit: BoxFit.cover,
                width:3/5 * MediaQuery.of(context).size.width,
              ),
            ),
            InkWell(
              onTap: () {
                setState(() {
                  index = index ==4 ? 1 :index +1;
                });
              },
              child: const Icon(
                Icons.arrow_right,
                color: Colors.green,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

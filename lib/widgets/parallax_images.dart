import 'package:flutter/material.dart';

class ParallaxImages extends StatefulWidget {
  const ParallaxImages(
      {Key? key,
      required this.imagePath,
      required this.height,
      required this.width,
      required this.index})
      : super(key: key);
  final String imagePath;
  final double height;
  final double width;
  final int index;
  @override
  State<ParallaxImages> createState() => _ParallaxImagesState();
}

class _ParallaxImagesState extends State<ParallaxImages> {
  final GlobalKey _imageKey = GlobalKey();
  @override
  Widget build(BuildContext context) {
    double height = widget.height;
    double width = widget.width;
    return Flow(
      delegate: ParallaxFlowDelegate(
        scrollable: Scrollable.of(context),
        imageKey: _imageKey,
        imageContext: context,
        index: widget.index,
      ),
      children: [
        Image.asset(
          widget.imagePath,
          height: height * 2 / 3,
          width: width,
          fit: BoxFit.cover,
          key: _imageKey,
        ),
      ],
    );
  }
}

class ParallaxFlowDelegate extends FlowDelegate {
  ParallaxFlowDelegate({
    required this.scrollable,
    required this.imageContext,
    required this.imageKey,
    required this.index,
  }) : super(repaint: scrollable.position);
  final ScrollableState scrollable;
  final BuildContext imageContext;
  final GlobalKey imageKey;
  final int index;
  @override
  BoxConstraints getConstraintsForChild(int i, BoxConstraints constraints) {
    return BoxConstraints.tightFor(
      width: constraints.maxWidth,
    );
  }

  @override
  void paintChildren(FlowPaintingContext context) {
    final viewportDimension = scrollable.position.pixels/(index/4+1);
    context.paintChild(
      0,
      transform:
          Transform.translate(offset: Offset(1.0,viewportDimension)).transform,
    );
  }

  @override
  bool shouldRepaint(ParallaxFlowDelegate oldDelegate) {
    return scrollable != oldDelegate.scrollable ||
        imageContext != oldDelegate.imageContext ||
        imageKey != oldDelegate.imageKey;
  }
}

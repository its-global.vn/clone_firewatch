import 'package:clone_firewatch/widgets/custom_text.dart';
import 'package:flutter/material.dart';

class CustomButton extends StatefulWidget {
  final String? imageIconPath;
  final String tittle;
  final double? height;
  final double? width;
  final double? textSize;
  final Color? backgroundColor;
  final Color? onFocusColor;
  final Color? textColor;
  final Function()? onTap;

  const CustomButton(
      {Key? key, this.imageIconPath,
      required this.tittle,
      this.height,
      this.width,
      this.backgroundColor,
      this.onFocusColor,
      this.textSize,
      this.textColor,
      this.onTap})
      : super(key: key);

  @override
  State<CustomButton> createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton> {
  @override
  Widget build(BuildContext context) {

    final String? iconPath = widget.imageIconPath;
    final double buttonHeight = widget.height ?? 60;
    final double buttonWidth = widget.width ?? MediaQuery. of(context). size. width*2/3;
    return Padding(
      padding: const EdgeInsets.only(top: 20.0, bottom: 20, left: 35, right: 35),
      child: SizedBox(
        height: buttonHeight,
        width: buttonWidth,
        child: Material(
          color: const Color.fromRGBO(151, 55, 0, 1),
          child: InkWell(
            splashColor: Colors.yellowAccent,
            onLongPress: () {
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Image(
                    image: AssetImage(
                        "assets/images/banner_transparent_left.png")),
                widget.imageIconPath != null ? Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Image(image: AssetImage("assets/images/$iconPath"),),
                ) : const SizedBox(),
                Flexible(
                    child: Align(
                      alignment: Alignment.center,
                      child: CustomText(
                  tittle: widget.tittle,
                  fontWeight: FontWeight.bold,
                  size: widget.textSize ?? 18,
                  color: widget.textColor,
                  ),
                    ),
                ),
                const Image(
                  image: AssetImage(
                      "assets/images/banner_transparent_right.png"),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

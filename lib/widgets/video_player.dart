import 'package:flutter/material.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
class VideoPlayerScreen extends StatefulWidget {
  const VideoPlayerScreen({Key? key}) : super(key: key);

  @override
  State<VideoPlayerScreen> createState() => _VideoPlayerScreenState();
}

class _VideoPlayerScreenState extends State<VideoPlayerScreen> {
  final videoId = YoutubePlayer.convertUrlToId("https://youtu.be/cXWlgP5hZzc");
  late YoutubePlayerController controller;

  @override
  void initState(){
    super.initState();
    controller = YoutubePlayerController(
        initialVideoId: videoId!,
        flags: const YoutubePlayerFlags(
          autoPlay: false,
        ),
    );

  }
  @override
  Widget build(BuildContext context) {
    return YoutubePlayer(
        controller: controller,
      showVideoProgressIndicator: true,
    );
  }
}
